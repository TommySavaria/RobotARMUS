#include <stdlib.h>

#include "headers/main.h"
#include "headers/jeu.h"

int execOperation(Operation op, int z, int min1, int max1, int min2, int max2);
void chaineOperation(Operation op, int nombre1, int nombre2, int nombre3);
int resultatOperation(Operation op, int nombre1, int nombre2, int nombre3);
void afficheQuestion();

int Niveau1();
int Niveau2();
int Niveau3();
int Niveau4();
int Niveau5();
int Niveau6();

char cNiveau;

int numQuestion;
int nombreQuestions;
char chaineQuestion[32];

int reponse;
int nombreClavier;

int timestampQuestion;
int timer = 0;
int nombreTimeout = 0;

bool repondu = false;
bool bonneReponse = false;
bool quitterJeu = false;

int jouer(unsigned int niveau, int deplacement)
{
	int y;
	unsigned int sonReacteur;
	unsigned char c;
	nombreQuestions = getNombreQuestionParNiveau();

	quitterJeu = false;

	if((niveau < 0) || (niveau > 6))
		return 0;

	while(!quitterJeu)
	{
		cNiveau = '0' + niveau;
		numQuestion = 0;

		while (numQuestion < nombreQuestions)
		{
			AUDIO_PlayFile("calculation.wav");

			switch(niveau)
			{
				case 1:
					y = Niveau1();
					break;
				case 2:
					y = Niveau2();
					break;
				case 3:
					y = Niveau3();
					break;
				case 4:
					y = Niveau4();
					break;
				case 5:
					y = Niveau5();
					break;
				case 6:
					y = Niveau6();
					break;
			}

			if(quitterJeu)
				return -1;

			afficheQuestion();

			if (y == 1)
			{
				numQuestion++;
				AUDIO_PlayFile("success.wav");
			}
			else if(y == 0)
			{
				AUDIO_PlayFile("error.wav");
			}

			THREAD_MSleep(DELAI_QUESTION);

		}

		LCD_ClearAndPrint("Bien joue, vous avez termine le niveau %d!\n\n", niveau);

		if(deplacement)
		{
			//Joue le son de d�placement
			sonReacteur = AUDIO_PlayFile("jet2.wav");

			//Deplacement
			THREAD_MSleep(3000);

			//�teint les DELS de la tuy�re
			ecrireDEL(0);

			AUDIO_StopPlayback(sonReacteur);
		}

		if(niveau < 6)
		{
			LCD_Printf("Passer au niveau %d?\n\n", niveau+1);
			LCD_Printf("Oui: Enter\n");
			LCD_Printf("Non: <--\n");
			LCD_Printf("Quitter: Esc");

			do
			{
				c = lireCaractere();
			} while((c != ENTER) && (c != BACKSPACE) && (c != ESCAPE));

			switch(c)
			{
				case ENTER:
					niveau++;
					break;
				case ESCAPE:
					quitterJeu = true;
					break;
			}
		}
		else if(niveau == 6)
		{
			LCD_Printf("Continuer?\n\n");
			LCD_Printf("Oui: Enter\n");

			switch(lireCaractere())
			{
				case ENTER:
					break;
				default:
					quitterJeu = true;
					break;
			}
		}
	}

	return 0;
}

// Fonctions de niveaux
int Niveau1()
{
	return execOperation(Addition, 0, 1, 9, 0, 0);
}

int Niveau2()
{
	switch(nombreAleatoire(3))
	{
		case 0:
			return execOperation(Addition, 0, 0, 49, 0, 0);
			break;
		case 1:
			return execOperation(Soustraction, 0, 0, 49, 0, 0);
			break;
		case 2:
			return execOperation(Comparaison, 0, 0, 50, 0, 0);
			break;
	}

	return 0;
}

int Niveau3()
{
	switch(nombreAleatoire(4))
	{
		case 0:
			return execOperation(Addition, 0, 0, 99, 0, 0);
			break;
		case 1:
			return execOperation(Soustraction, 0, 0, 99, 0, 0);
			break;
		case 2:
			return execOperation(Comparaison, 0, 0, 99, 0, 0);
			break;
		case 3:
			return execOperation(Multiplication, 0, 0, 9, 0, 0);
			break;
	}

	return 0;
}

int Niveau4()
{
	switch(nombreAleatoire(5))
	{
		case 0:
			return execOperation(Addition, 0, 0, 200, 0, 0);
			break;
		case 1:
			return execOperation(Soustraction, 0, 0, 200, 0, 0);
			break;
		case 2:
			return execOperation(Comparaison, 0, 0, 200, 0, 0);
			break;
		case 3:
			return execOperation(Multiplication, 0, 0, 9, 0, 0);
			break;
		case 4:
			return execOperation(Priorite, 4, 0, 99, 0, 9);
			break;
	}

	return 0;
}

int Niveau5()
{
	switch(nombreAleatoire(6))
	{
		case 0:
			return execOperation(Addition, 0, 0, 200, 0, 0);
			break;
		case 1:
			return execOperation(Soustraction, 0, 0, 200, 0, 0);
			break;
		case 2:
			return execOperation(Division, 0, 0, 12, 0, 0);
			break;
		case 3:
			return execOperation(Multiplication, 0, 0, 12, 0, 0);
			break;
		case 4:
			return execOperation(Priorite, 8, 0, 50, 0, 9);
			break;
		case 5:
			return execOperation(Puissance, 0, 0, 5, 0, 3);
			break;
	}

	return 0;
}

int Niveau6()
{
	switch(nombreAleatoire(6))
	{
		case 0:
			return execOperation(Addition, 0, 0, 200, 0, 0);
			break;
		case 1:
			return execOperation(Soustraction, 0, 0, 200, 0, 0);
			break;
		case 2:
			return execOperation(Division, 0, 0, 15, 0, 0);
			break;
		case 3:
			return execOperation(Multiplication, 0, 0, 15, 0, 0);
			break;
		case 4:
			return execOperation(Priorite, 8, 0, 100, 0, 12);
			break;
		case 5:
			return execOperation(Puissance, 0, 0, 5, 0, 3);
			break;
	}

	return 0;
}

int NiveauImpossible()
{
	int y;

	nombreQuestions = 2;
	numQuestion = 0;
	cNiveau = 'i';
	quitterJeu = false;

	while(numQuestion < nombreQuestions)
	{
		switch(numQuestion)
		{
			case 0:
				y = execOperation(Euler, 0, 0, 0, 0, 0);
				break;
			case 1:
				y = execOperation(ExpSinPi, 0, 0, 0, 0, 0);
				break;
		}

		if(quitterJeu)
			return -1;

		if (y == 1)
			numQuestion++;
	}

	LCD_ClearAndPrint("Bien joue, vous avec gagne\n\n");
	return 1;
}

int execOperation(Operation op, int z, int min1, int max1, int min2, int max2)
{
	int nombre1, nombre2, nombre3;
	int rPriorite, reponseClavier;

	repondu = false;
	quitterJeu = false;
	nombre1 = nombreAleatoire(min1, max1);

	switch(op)
	{
		case Puissance:
			//Utilise min1 - max1 pour la base
			//Utilise min2 - max2 pour l'exposant
			nombre2 = nombreAleatoire(min2, max2);
			break;
		case Priorite:
			//S�lectionne une des 8 posssibilit�es pour l'op�ration de priorit�
			rPriorite = nombreAleatoire(z);
			switch(rPriorite)
			{
				case 0:
					op = Priorite1;
					break;
				case 1:
					op = Priorite2;
					break;
				case 2:
					op = Priorite3;
					break;
				case 3:
					op = Priorite4;
					break;
				case 4:
					op = Priorite5;
					break;
				case 5:
					op = Priorite6;
					break;
				case 6:
					op = Priorite7;
					break;
				case 7:
					op = Priorite8;
					break;
			}

			//Utilise min2 - max2 pour les deux nombres suivants
			nombre2 = nombreAleatoire(min2, max2);
			nombre3 = nombreAleatoire(min2, max2);
			break;

		case Comparaison:

			//S�lectionne plus grand ou plus petit
			rPriorite = nombreAleatoire(2);
			if(rPriorite == 0)
				op = PlusPetit;
			else
				op = PlusGrand;

			nombre2 = nombreAleatoire(min1, max1);
			break;

		default:
			nombre2 = nombreAleatoire(min1, max1);
			break;
	}

	//Affiche l'op�ration
	chaineOperation(op, nombre1, nombre2, nombre3);
	reponse = resultatOperation(op, nombre1, nombre2, nombre3);
	afficheQuestion();

	timestampQuestion = armus::system_GetTimeStamp();
	timer = getDelaiQuestion();

	//Lit la r�ponse de l'utilisateur
	reponseClavier = lireNombre();
	if(quitterJeu)
		return -1;

	//V�rifie la r�ponse et affiche bonne r�ponse ou mauvaise r�ponse � l'utilisateur
	//Attend pendant DELAI_QUESTION millisecondes
	repondu = true;
	bonneReponse = (reponse == reponseClavier);

	//Retourne 1 si la r�ponse est bonne
	if(bonneReponse)
		return 1;

	//Retourne 0 sinon
	else
		return 0;
}

//Cr�� la chaine de caract�res pour la question selon l'op�ration s�lectionn�e
//Et les 3 nombres choisis au hasard
void chaineOperation(Operation op, int nombre1, int nombre2, int nombre3)
{
	switch(op)
	{
		case Addition:
			sprintf(chaineQuestion, "%d + %d = ? ", nombre1, nombre2);
			break;
		case Soustraction:
			sprintf(chaineQuestion, "%d - %d = ? ", max(nombre1, nombre2), min(nombre1, nombre2));
			break;
		case Multiplication:
			sprintf(chaineQuestion, "%d * %d = ? ", nombre1, nombre2);
			break;
		case Division:
			sprintf(chaineQuestion, "%d / %d = ? ", nombre1 * nombre2, nombre2);
			break;
		case PlusPetit:
			sprintf(chaineQuestion, "Le plus petit entre %d et %d ", nombre1, nombre2);
			break;
		case PlusGrand:
			sprintf(chaineQuestion, "Le plus grand entre %d et %d ", nombre1, nombre2);
			break;
		case Puissance:
			sprintf(chaineQuestion, "%d ^ %d = ? ", nombre1, nombre2);
			break;
		case Priorite1:
			sprintf(chaineQuestion, "%d + %d * %d = ? ", nombre1, nombre2, nombre3);
			break;
		case Priorite2:
			sprintf(chaineQuestion, "%d * %d + %d = ? ", nombre3, nombre2, nombre1);
			break;
		case Priorite3:
			sprintf(chaineQuestion, "%d - %d * %d = ? ", nombre1, nombre2, nombre3);
			break;
		case Priorite4:
			sprintf(chaineQuestion, "%d * %d - %d = ? ", nombre3, nombre2, nombre1);
			break;
		case Priorite5:
			sprintf(chaineQuestion, "%d + %d / %d = ? ", nombre1, nombre2 * nombre3, nombre3);
			break;
		case Priorite6:
			sprintf(chaineQuestion, "%d / %d + %d = ? ", nombre3 * nombre2, nombre2, nombre1);
			break;
		case Priorite7:
			sprintf(chaineQuestion, "%d - %d / %d = ? ", nombre1, nombre2 * nombre3, nombre3);
			break;
		case Priorite8:
			sprintf(chaineQuestion, "%d / %d - %d = ? ", nombre3*nombre2, nombre2, nombre1);
			break;
		case Euler:
			sprintf(chaineQuestion, "e ^ (i * pi) - 1 = ? ");
			break;
		case ExpSinPi:
			sprintf(chaineQuestion, "e ^ (sin(pi)) ");
			break;
	}
}

//Calcul le r�sultat selon l'op�ration et les 3 nombres choisis au hasard
int resultatOperation(Operation op, int nombre1, int nombre2, int nombre3)
{
	int r;

	switch(op)
	{
		case Addition:
			return nombre1 + nombre2;
			break;
		case Soustraction:
			return max(nombre1, nombre2) - min(nombre1, nombre2);
			break;
		case Multiplication:
			return nombre1 * nombre2;
			break;
		case Division:
			return nombre1;
			break;
		case PlusPetit:
			return min(nombre1, nombre2);
			break;
		case PlusGrand:
			return max(nombre1, nombre2);
			break;
		case Puissance:
			r = 1;

			for(int i = 0; i < nombre2; i++)
					r *= nombre1;

			return r;
			break;
		case Priorite1:
			return nombre3 * nombre2 + nombre1;
			break;
		case Priorite2:
			return nombre1 + nombre2 * nombre3;
			break;
		case Priorite3:
			return nombre1 - nombre2 * nombre3;
			break;
		case Priorite4:
			return nombre3 * nombre2 - nombre1;
			break;
		case Priorite5:
			return nombre1 + nombre2;
			break;
		case Priorite6:
			return nombre3 + nombre1;
			break;
		case Priorite7:
			return nombre1 - nombre2;
			break;
		case Priorite8:
			return nombre3 - nombre1;
			break;
		case Euler:
			return 0;
			break;
		case ExpSinPi:
			return 1;
			break;
	}

	return 0;
}

//Affiche la question � l'�cran, le nombre entr� au clavier
//Et une barre de statut avec niveau, question et temps
void afficheQuestion()
{
	int a, b, c;
	char ligne[32];

	LCD_ClearAndPrint("%s\n", chaineQuestion);
	LCD_Printf("\n");
	LCD_Printf("%d\n", nombreClavier);
	LCD_Printf("\n");

	if(repondu)
	{
		if(bonneReponse)
			LCD_Printf("Bonne reponse!\n\n");
		else
			LCD_Printf("Mauvaise reponse\nLa reponse est %d\n", reponse);
	}
	else
	{
		LCD_Printf("\n\n");
	}

	LCD_Printf("\n");
	LCD_Printf("Niveau %c       %1d/%1d           %02d", cNiveau, numQuestion + 1, nombreQuestions, timer);
}

int updateTimer()
{
	return getDelaiQuestion() - armus::system_ReadTimer_sec(timestampQuestion);
}

//Lit le nombre au clavier
int lireNombre()
{
	char c;
	int nombrePrecedent = 0;
	int timerPrecedent = timer;
	bool done = false;
	nombreClavier = 0;

	afficheQuestion();
	pulserDELS();

	while(!done)
	{
		c = lireCaractere();

		if(c == ESCAPE)
		{
			//Quitte la partie
			quitterJeu = true; //Flag retourner au menu principal
			done = true; //On quitte la boucle
		}

		else if(c == BACKSPACE)
		{
			//Soustrait le dernier chiffre
			//Si c'est plus petit que 0, remm�ne � 0
			nombreClavier /= 10;
		}

		else if(c == ENTER)
		{
			//Quitte la fonction avec la valeur actuelle
			nombreTimeout = 0;
			done = true;
		}

		else if(c == '-')
		{
			//Change le signe
			nombreClavier = -(nombreClavier);
		}

		else if(isdigit(c)) //Si on appuie sur un chiffre
		{
			//Ajuste le nombre
			if(abs(nombreClavier) <= 9999) //Limite � 5 chiffres
				nombreClavier = (nombreClavier * 10) + (c - '0');
		}

		//Mettre � jour le timer
		timer = updateTimer();

		//V�rifie si le d�lai est �coul�
		if(timer == 0)
		{
			done = true;

			if(++nombreTimeout >= 3)
				quitterJeu = true;
		}

		//Met � jour l'affichage si le nombre ou le timer ont chang�
		if(nombreClavier != nombrePrecedent)
		{
			afficheQuestion();
			nombreTimeout = 0;
		}

		if(timerPrecedent != timer)
		{
			afficheQuestion();
		}

		nombrePrecedent = nombreClavier;
		timerPrecedent = timer;
	}

	return nombreClavier;
}

int nombreAleatoire(int nombre)
{
	//return rand() % nombre;
	return SYSTEM_Random(nombre);
}

int nombreAleatoire(int min, int max)
{
	//return (rand() % (max - min + 1)) + min;
	return SYSTEM_Random(max - min + 1) + min;
}
