/*
============================================================================
 Name : robot.cpp
 Author : Guillaume Hivert, Anthony Fortin, Wendy Vasquez, Keven Deslandes,
            Tommy Savaria, �tienne St-Pierre
 Description : Code pour le d�placement des robots
============================================================================
*/

#include "headers/main.h"

//D�finitions sp�cifiques au robot A
#ifdef ROBOT_A
#define RATIO_GAUCHE 1.0
#define RATIO_DROITE 0.90
#define COMPENSATION_VIRAGE 1.65   // 1.73 avec fil
#endif

//D�finitions sp�cifiques au robot B
#ifdef ROBOT_B
#define RATIO_GAUCHE 0.90
#define RATIO_DROITE 1.0
#define COMPENSATION_VIRAGE 1.65 // non test� avec fil
#endif

//D�finitions communes aux 2 robots
#define LONGUEUR_PAR_IMPULSION 0.0037
#define DISTANCE_ENTRE_ROUES 0.14

#define FACTEUR_PONDERATION_PROPORTIONNEL 0.75
#define FACTEUR_PONDERATION_INTEGRALE 0.75

#define GRAND_RAYON .295
#define PETIT_RAYON .155

void resetEncodeurs();
double calculCompensation(int Encodeur1, int Encodeur2, int* pointeurErreur, double vitesseActuelle);
double calculCompensationArc(int encodeurPetit, int encodeurGrand, int* pointeurErreur, int grandeVitesse);
double calculDistanceParcourue(int encodeurGauche, int encodeurDroit, double* angle);
void CalculTournArc(double angledeg, double grandeVitesse, double result[4]);

int lireCapteurs(int flagsArret)
{
	char c;

	//V�rifie les capteurs
	if((lireTensionDistance() >= 1.1) && (flagsArret & STOP_ON_GP2))
		return 1;

	if((IR_Detect(IR_FRONT) != 0) && (flagsArret & STOP_ON_IR_FRONT))
		return 2;

	if((IR_Detect(IR_BACK) != 0) && (flagsArret & STOP_ON_IR_BACK))
		return 3;

	if((capteurCouleur() > 800) && (capteurCouleur() < 950) && (flagsArret & STOP_ON_COLOR))
		return 4;

	c = lireCaractere();
	if((c != 255) &&  (flagsArret & STOP_ON_KEYBOARD))
		return c;

	return 0;
}

int robotAvance(double distance, int vitesseInitiale, int correctionActivee, int flagsArret)
{
	int encodeurGauche = 0, encodeurDroit = 0; //Valeurs lues des encodeurs
	double vitesseDroit = vitesseInitiale*RATIO_DROITE; //Vitesse droite initiale
	double vitesseGauche = vitesseInitiale*RATIO_GAUCHE; //Vitesse gauche initiale
	int sommeErreur = 0; //Int�grale de l'erreur
	int capteur = 0;

	double angleLigne  = 0;
	double distanceParcourue = 0; //Distance parcourue depuis le d�but de l'appel de fonction
	double distanceApproximee = 0;

	resetEncodeurs();
	setVitesseMoteurs(vitesseGauche, vitesseDroit);
	SYSTEM_ResetTimer();

	while((distanceParcourue + distanceApproximee) < distance)
	{
		//V�rifier la tension de batterie
		verifierTension();

		//Fait pulser les DELS
		pulserDELS();

		capteur = lireCapteurs(flagsArret);
		if(capteur != 0)
			return capteur;

		//Lit les encodeurs
		encodeurGauche += ENCODER_Read(ENCODER_LEFT);
		encodeurDroit += ENCODER_Read(ENCODER_RIGHT);

		//Si le temps �coul� est plus de 500ms
		if(SYSTEM_ReadTimerMSeconds() > 500.0 && correctionActivee)
		{
			SYSTEM_ResetTimer();

			//Calcule la compensation et change la vitesse
			vitesseDroit = calculCompensation(encodeurGauche, encodeurDroit, &sommeErreur, vitesseDroit);
			setVitesseMoteurs(vitesseGauche, vitesseDroit);
			//LCD_Printf("Gauche: %d, Droite: %d\n", vitesseGauche, vitesseDroit);

			//Calcule la distance parcourue avec la moyenne des encodeurs
			distanceParcourue += calculDistanceParcourue(encodeurGauche, encodeurDroit, &angleLigne);
			//LCD_Printf("Distance: %f\n", distanceParcourue);

			encodeurGauche = 0;
			encodeurDroit = 0;
		}

		distanceApproximee = ((encodeurGauche + encodeurDroit) / 2.0) * LONGUEUR_PAR_IMPULSION;
	}

	//�teint les moteurs
	arreterMoteurs();
	return 0;
}

int robotArc(double angle, int vitesseInitiale, int direction, int flagsArret)
{
	int encodeurGauche = 0, encodeurDroit = 0; //Valeurs lues des encodeurs
	int capteurs = 0;
	double vitesseDroit, vitesseGauche; //Vitesse
	double distance;//Distance Parcourue;
	double angleParcouru = 0;
	double resultat[4];

	CalculTournArc(angle, vitesseInitiale, resultat);

	if(direction == GAUCHE)
	{
		vitesseDroit = resultat[3];
		vitesseGauche = resultat[2];
	}
	else
	{
		vitesseDroit = resultat[2];
		vitesseGauche = resultat[3];
	}

	resetEncodeurs();
	setVitesseMoteurs(vitesseGauche, vitesseDroit);

	while(angleParcouru < angle)
	{
		//V�rifier la tension de batterie
		verifierTension();

		//Fait pulser les DELS
		pulserDELS();

		capteurs = lireCapteurs(flagsArret);
		if(capteurs != 0)
			return capteurs;

		//Lit les encodeurs
		encodeurGauche += ENCODER_Read(ENCODER_LEFT);
		encodeurDroit += ENCODER_Read(ENCODER_RIGHT);

		//Calcul de l'angle parcouru
		if(direction == GAUCHE)
		{
			distance = (encodeurDroit - encodeurGauche) * LONGUEUR_PAR_IMPULSION;
			angleParcouru = radToDeg(distance / DISTANCE_ENTRE_ROUES);
		}
		else
		{
			distance = (encodeurGauche - encodeurDroit) * LONGUEUR_PAR_IMPULSION;
			angleParcouru = radToDeg(distance / DISTANCE_ENTRE_ROUES);
		}
	}

	//�teint les moteurs
	arreterMoteurs();
	return 0;
}

//Fonction pour rotation du robot sur lui-m�me
int robotTourne(int angle, int vitesse, int flagsArret)
{
    int encodeurGauche = 0, encodeurDroit = 0;
    int capteurs = 0;
    double distanceApproximee = 0;
    double angleParcouru = 0, angleRadians = 2.0*degToRad(angle);

    resetEncodeurs();
    setVitesseMoteurs(-vitesse*RATIO_GAUCHE, vitesse*RATIO_DROITE);

    //while(distanceApproximee < d_180)
    while(angleParcouru < angleRadians)
    {
    	//V�rifier la tension de batterie
    	verifierTension();

		//Fait pulser les DELS
		pulserDELS();

		capteurs = lireCapteurs(flagsArret);
		if(capteurs != 0)
			return capteurs;

    	//Lit les encodeurs
    	encodeurGauche += ENCODER_Read(ENCODER_LEFT);
    	encodeurDroit += ENCODER_Read(ENCODER_RIGHT);

    	angleParcouru = (encodeurGauche + encodeurDroit) * 2 * (LONGUEUR_PAR_IMPULSION / DISTANCE_ENTRE_ROUES);
    }

    //�teint les moteurs
    arreterMoteurs();
    return 0;
}

void resetEncodeurs()
{
	//Lit les encodeurs pour les resetter
	ENCODER_Read(ENCODER_LEFT);
	ENCODER_Read(ENCODER_RIGHT);
}

void setVitesseMoteurs(double gauche, double droite)
{
	//Set la vitesse des moteurs
	MOTOR_SetSpeed(MOTOR_LEFT, (int)gauche);
	MOTOR_SetSpeed(MOTOR_RIGHT, (int)droite);
}

void arreterMoteurs()
{
	//Set la vitesse des moteurs
	MOTOR_SetSpeed(MOTOR_LEFT, 0);
	MOTOR_SetSpeed(MOTOR_RIGHT, 0);
	ecrireDEL(0);
}

double calculCompensation(int Encodeur1, int Encodeur2, int* pointeurErreur, double vitesseActuelle)
{
	//Definission des variable de l'integrateur
	int differenceClics;
	double ajustementVitesseProportionel, ajustementVitesseIntegrale;
	int sommeErreur = *pointeurErreur;
	double vitesseNouvelle = vitesseActuelle;

	differenceClics = Encodeur1 - Encodeur2;
	ajustementVitesseProportionel = differenceClics*FACTEUR_PONDERATION_PROPORTIONNEL;

	sommeErreur += differenceClics;
	ajustementVitesseIntegrale = sommeErreur*FACTEUR_PONDERATION_INTEGRALE;

	vitesseNouvelle += ajustementVitesseProportionel + ajustementVitesseIntegrale;

	//Clamp des valeurs de vitesse
	if (vitesseNouvelle > 100)
		vitesseNouvelle = 100;
	if (vitesseNouvelle < -100)
		vitesseNouvelle = -100;

	*pointeurErreur = sommeErreur; //Pointeur Sommation Erreur
	return vitesseNouvelle;
}

double calculCompensationArc(int encodeurPetit, int encodeurGrand, int* pointeurErreur, int grandeVitesse)
{
	return calculCompensation(encodeurGrand, encodeurPetit*1.7, pointeurErreur, grandeVitesse);
}

double calculDistanceParcourue(int encodeurGauche, int encodeurDroit, double* angle)
{
	double differenceEncodeurs = encodeurDroit - encodeurGauche;
	double moyenneEncodeurs = (encodeurDroit + encodeurGauche) / 2.0;
	*angle += (differenceEncodeurs * LONGUEUR_PAR_IMPULSION) / DISTANCE_ENTRE_ROUES;
	return (moyenneEncodeurs * LONGUEUR_PAR_IMPULSION) * cos(*angle);
}

void CalculTournArc(double angledeg, double grandeVitesse, double result[4])
{
	double anglerad = degToRad(angledeg);
	double petitArc = anglerad*PETIT_RAYON;
	double grandArc = anglerad*GRAND_RAYON;
	double petiteVitesse = grandeVitesse / COMPENSATION_VIRAGE;

	result[0] = petitArc;
	result[1] = grandArc;
	result[2] = petiteVitesse;
	result[3] = grandeVitesse;
}
