#include "headers/main.h"

#define INTENSITE_BASSE 30
#define INTENSITE_HAUTE 80

void ecrireDEL(unsigned char intensite)
{
	MOTOR_SetSpeed(6, intensite);
}

void changerEtatDels()
{
	int intensite;
	static int timestampDEL = armus::system_GetTimeStamp();

	if ((intensite == INTENSITE_BASSE) && (armus::system_ReadTimerMSeconds(timestampDEL) > 500))
	{
		ecrireDEL(INTENSITE_HAUTE);
		timestampDEL = armus::system_GetTimeStamp();
	}
	else if ((intensite == INTENSITE_HAUTE) && (armus::system_ReadTimerMSeconds(timestampDEL) > 500))
	{
		ecrireDEL(INTENSITE_BASSE);
		timestampDEL = armus::system_GetTimeStamp();
	}
}

void pulserDELS()
{
	int t0 = 0, t1 = 0, val;
	static int timestamp = armus::system_GetTimeStamp();

	t0 = armus::system_ReadTimerMSeconds(timestamp) / 100;
	if(t0 > t1)
	{
		if(t0 >= 10)
			t0 %= 10;

		t1 = t0;
		val = (sin(t0 * M_PI / 5) + 1) * 50;
		ecrireDEL(val);
	}
}
