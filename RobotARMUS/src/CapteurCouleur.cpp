#include <libarmus.h>
#include "headers/main.h"

#define ADJD_S371_QR999_SADR 	0x74
#define CAP_RED					0x6
#define CAP_GREEN				0x7
#define CAP_BLUE				0x8
#define CAP_CLEAR				0x9
#define INTEGRATION_RED			10
#define INTEGRATION_GREEN		12
#define INTEGRATION_BLUE		14
#define INTEGRATION_CLEAR		16
#define ADJD_REG_CTRL			0
#define ADJD_REG_CONFIG			1
#define DATA_RED_LO				64
#define DATA_GREEN_LO			66
#define DATA_BLUE_LO			68
#define DATA_CLEAR_LO			70
#define CAP_AMOUNT              7

#define CTRL_GSSR				0
#define CTRL_GOFS				1

#define CONFIG_TOFS				0

#define TOLERANCE 				0.08


int adjd_dev;
char* nomCouleur[8] = {"Rouge", "Vert", "Bleu", "Jaune", "Rose", "Gris", "Blanc", "Noir"};

// fonctions globales

int Capteur_Couleur();
float chromaConvert(float R, float G, float B, float max, float min, float delta);

//permet de changer la valeur des registres
void adjd_SetRegister(unsigned char reg, unsigned char val)
{
	unsigned char data[2];
	data[0] = reg;
	data[1] = val;
	armus::i2c_Write(adjd_dev, 2, data);
}

//permet de changer la valeur des registres de 16 bits
void adjd_SetRegister16(unsigned char reg, int val)
{
	unsigned char data[2];
	data[0] = reg;
	data[1] = val & 0xFF;
	armus::i2c_Write(adjd_dev, 2, data);
	data[0] = reg+1;
	data[1] = (val >> 8) & 0xFF;
	armus::i2c_Write(adjd_dev, 2, data);
}

//permet de lire la valeur des registres
unsigned char adjd_ReadRegister(unsigned char reg)
{
	unsigned char val;

	armus::i2c_ReadAfterWrite(adjd_dev, 1, &reg, 1, &val);

	return val;
}

//permet de lire la valeur des registres de 16 bits
int adjd_ReadRegister16(unsigned char reg)
{
	int val16;
	unsigned char val;
	armus::i2c_ReadAfterWrite(adjd_dev, 1, &reg, 1, &val);
	val16 = val;
	reg = reg+1;
	armus::i2c_ReadAfterWrite(adjd_dev, 1, &reg, 1, &val);
	val16 = val16 + ((val << 8) & 0xFF00);
	return val16;
}


// Permet de connaitre la valeur du CAP dans le registre
// prend comme argument une constante CAP_RED, CAP_BLUE, CAP_CLEAR ou CAP_GREEN
// retourne un unsigned char de la valeur
unsigned char cap_GetValue(unsigned char cap_address)
{
	unsigned char cap_value;

	cap_value = adjd_ReadRegister(cap_address);

	return cap_value;
}


// Permet de changer la valeur du CAP dans le registre
// prend comme premier argument une constante CAP_RED, CAP_BLUE, CAP_CLEAR ou CAP_GREEN
// le second argument est compris entre 0 et 15, et determine la valeur du cap a ecrire dans le registre
void cap_SetValue(unsigned char cap_address, unsigned char cap_value)
{
	adjd_SetRegister(cap_address, cap_value);
}



// Permet de connaitre la valeur du CAP dans le registre
// address est une constante comme INTEGRATION_RED, ...
// retourne un int de la valeur
int integrationTime_GetValue(unsigned char address)
{
	int time_value;

	time_value = adjd_ReadRegister16(address);

	return time_value;
}


// Permet de changer la valeur du CAP dans le registre
// address est une constante comme INTEGRATION_RED, ...
// time_value est compris entre 0 et 4095
void integrationTime_SetValue(unsigned char address, int time_value)
{
	adjd_SetRegister16(address, time_value);
}


// Vous devez vous-meme modifier cette fonction tout dependamment de la sortie numerique utilisee
void led_TurnOff()
{
	// TODO : code a changer
	DIGITALIO_Write(9, 0);
}

// Vous devez vous-meme modifier cette fonction tout dependamment de la sortie numerique utilisee
void led_TurnOn()
{
	// TODO : code a changer
	DIGITALIO_Write(9, 1);
}

// permet de faire une lecture differentielle avec et sans eclairage de la led
void color_Read(int& data_red, int& data_blue, int& data_green, int& data_clear)
{
	//premiere lecture sans eclairage
	led_TurnOff();

	adjd_SetRegister(ADJD_REG_CONFIG, 1 << CONFIG_TOFS);
	adjd_SetRegister(ADJD_REG_CTRL, 1 << CTRL_GOFS);
	while(adjd_ReadRegister(ADJD_REG_CTRL))
	{
		THREAD_MSleep(50);
	}

	//lecture avec eclairage
	led_TurnOn();
	adjd_SetRegister(ADJD_REG_CTRL, 1 << CTRL_GSSR);
	while(adjd_ReadRegister(ADJD_REG_CTRL))
	{
		THREAD_MSleep(50);
	}

	//eteindre la led
	led_TurnOff();

	data_red = adjd_ReadRegister16(DATA_RED_LO);
	data_green = adjd_ReadRegister16(DATA_GREEN_LO);
	data_blue = adjd_ReadRegister16(DATA_BLUE_LO);
	data_clear = adjd_ReadRegister16(DATA_CLEAR_LO);
}

void color_ReadToCalibrate(int& data_red, int& data_blue, int& data_green, int& data_clear)
{
	led_TurnOn();
	adjd_SetRegister(ADJD_REG_CONFIG, 0 << CONFIG_TOFS);
	adjd_SetRegister(ADJD_REG_CTRL, 1 << CTRL_GSSR);
	while(adjd_ReadRegister(ADJD_REG_CTRL))
	{
		THREAD_MSleep(50);
	}
	led_TurnOff();

	data_red = adjd_ReadRegister16(DATA_RED_LO);
	data_green = adjd_ReadRegister16(DATA_GREEN_LO);
	data_blue = adjd_ReadRegister16(DATA_BLUE_LO);
	data_clear = adjd_ReadRegister16(DATA_CLEAR_LO);

}

// l argument est un integer qui ne doit pas etre modifie
int color_Init(int& dev_handle)
{
	int error;
	error = armus::i2c_RegisterDevice(ADJD_S371_QR999_SADR, (uint32_t)100000, (uint32_t)1000, dev_handle);

	return error;
}

Couleur convertRGB_HSL(int red, int green, int blue, float clear)
{

    float delta = 0;
    float luminosite = 0;
    Couleur couleur;
    float max = 0, min = 0;
    float hue = 0;

    float R = red/(clear);
    float G = green/(clear);
    float B = blue/(clear);

    // Calcul de Max, Min et Delta

    if (R >= G && R >= B)
    {
        max = R;
    }

    if (G >= R && G >= B)
    {
        max = G;
    }

    if (B >= R && B >= G)
    {
        max = B;
    }

    if (R <= G && R <= B)
    {
        min = R;
    }

    if (G <= R && G <= B)
    {
        min = G;
    }

    if (B <= R && B <= G)
    {
        min = B;
    }

    delta = max - min;


    // Calcul de la luminosite si c'est une nuance de blanc/gris/noir

    if (delta <= TOLERANCE*clear/2)
    {
        luminosite = (max + min) / 2;

        LCD_Printf("\nLum = %f", luminosite);

        if (luminosite < 0.33)
        {
            couleur = Noir;
        }
        else if (luminosite < 0.66)
        {
            couleur = Gris;
        }
        else
        {
            couleur = Blanc;
        }
        return couleur;
    }

    // Calcul de la teinte si c'est une couleur non blanche/grise/noire

    else
    {
        hue = chromaConvert(R, G, B, max, min, delta);

        LCD_Printf("\nHue = %f", hue);

        if (hue < 30)
        {
            couleur = Rouge;
        }
        else if (hue < 90)
        {
            couleur = Jaune;
        }
        else if (hue < 150)
        {
            couleur = Vert;
        }
        else if (hue < 270)
        {
            couleur = Bleu;
        }
        else if (hue <= 360)
        {
            couleur = Rose;
        }
        return couleur;
    }
}


float chromaConvert(float R, float G, float B, float max, float min, float delta)
{

    float H1 = 0;
    float H = 0;

    if (max == R)
    {
        float H2 = ((G - B) / delta);
        H1 = fmod(H2, 6);
    }

    if (max == G)
    {
        H1 = ((B - R) / delta) + 2;
    }

    if (max == B)
    {
        H1 = ((R - G) / delta) + 4;
    }

    H = 60 * H1;

    if (H > 360)
    {
        H = H - 360;
    }

    return H;
}

int capteurCouleur()
{
	int red, blue, green, clear;
	Couleur couleur;

	//initialisation du capteur
	ERROR_CHECK(color_Init(adjd_dev));

	cap_SetValue(CAP_RED, CAP_AMOUNT + 1);
	cap_SetValue(CAP_GREEN, CAP_AMOUNT + 2);
	cap_SetValue(CAP_BLUE, CAP_AMOUNT - 7);
	cap_SetValue(CAP_CLEAR, CAP_AMOUNT + 8);

	integrationTime_SetValue(INTEGRATION_RED, 255);
	integrationTime_SetValue(INTEGRATION_GREEN, 255);
	integrationTime_SetValue(INTEGRATION_BLUE, 255);
	integrationTime_SetValue(INTEGRATION_CLEAR, 255);

	color_Read(red, blue, green, clear);

//	couleur = convertRGB_HSL(red, green, blue, clear);

	return clear;
}

