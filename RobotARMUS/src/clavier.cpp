#include <ctype.h>

#include "headers/main.h"

const int colonnes_matrice[4] = {16, 15, 14, 13};
const int lignes_matrices[4] = {12, 11, 10, 9};

unsigned char etatPrecedent[4][4] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
unsigned char toucheAppuyee[4][4] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

void lireClavier()
{
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
			etatPrecedent[i][j] = toucheAppuyee[i][j];
	}


	for(int i = 0; i < 4; i++)
	{
		DIGITALIO_Write(colonnes_matrice[i], 1); //Met un niveau haut � la colonne correspondante

		for(int j = 0; j < 4; j++)
		{
			toucheAppuyee[j][i] = DIGITALIO_Read(lignes_matrices[j]); //Lit la ligne correspondante � la touche
		}

		DIGITALIO_Write(colonnes_matrice[i], 0); //Remet un niveau bass � la colonne correspondante
	}
}

unsigned char lireCaractere()
{
	static int i = 0;
	static int j = 0;
	bool done = false;

	//V�rifier la tension de batterie
	verifierTension();

	//Si on avait fait le tour la derni�re fois
	//Rescanner le clavier
	if((i == 0) && (j == 0))
		lireClavier();

	//V�rifier les touches appuy�es sur le clavier
	while(!done)
	{
		//Si on a appuy� sur une touche depuis la derni�re fois
		if((toucheAppuyee[i][j] != 0) && (etatPrecedent[i][j] == 0))
			done = true;

		if(++j >= 4)
		{
			j = 0;
			i++;

			if((i >= 4) && !done)
			{
				i = 5;
				done = true;
			}
		}
	}

	//Retourne le code ASCII correspondant � la touche
	switch((i * 4) + j - 1)
	{
		case 0:
			return '1';
			break;
		case 1:
			return '4';
			break;
		case 2:
			return '7';
			break;
		case 3:
			return 'R';
			break;
		case 4:
			return '2';
			break;
		case 5:
			return '5';
			break;
		case 6:
			return '8';
			break;
		case 7:
			return '0';
			break;
		case 8:
			return '3';
			break;
		case 9:
			return '6';
			break;
		case 10:
			return '9';
			break;
		case 11:
			return '-';
			break;
		case 12:
			return ESCAPE; //Escape
			break;
		case 13:
			return 'P';
			break;
		case 14:
			return BACKSPACE; //Backspace
			break;
		case 15:
			return ENTER; //Enter
			break;
		default: //Acucune touche appuy�e
			i = 0;
			j = 0;
			return 255;
			break;
	}

	return 255;
}
