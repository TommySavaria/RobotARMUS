/*
============================================================================
 Name : main.h
 Author : Guillaume Hivert, Anthony Fortin, Wendy Vasquez, Keven Deslandes,
            Tommy Savaria, �tienne St-Pierre
 Description : Fichier d'en-t�te
============================================================================
*/

#ifndef _MAIN_H_
#define _MAIN_H_

// Include Files
#include <math.h>
#include <libarmus.h>
#include <stdint.h>

//Macros
#define ROBOT_A
//#define ROBOT_B

#define GAUCHE 0
#define DROITE 1

#define ON 1
#define OFF 0

#define ENTER 10
#define ESCAPE 27
#define BACKSPACE 127

#define degToRad(a) ((M_PI * (a)) / 180)
#define radToDeg(a) ((180 * (a)) / M_PI)

#define STOP_ON_GP2 		(1 << 0)
#define STOP_ON_IR_FRONT 	(1 << 1)
#define STOP_ON_IR_BACK 	(1 << 2)
#define STOP_ON_COLOR		(1 << 3)
#define STOP_ON_KEYBOARD	(1 << 4)

inline int abs ( int a ) { return a < 0 ? -(a) : a; }
inline int max ( int a, int b ) { return a > b ? a : b; }
inline int min ( int a, int b ) { return a < b ? a : b; }

//batterie.cpp
double lireBatterie();
void allumeDELBasseTension();
void verifierTension();

//CapteurCouleur.cpp
int capteurCouleur();

//CapteurDistance.cpp
float lireDistance();
float lireTensionDistance();

//clavier.cpp
unsigned char lireCaractere();
int lireNombre();

//demo.cpp
void demoDEL();

//jeu.cpp
int nombreAleatoire(int nombre);
int nombreAleatoire(int min, int max);

//leds.cpp
void ecrireDEL(unsigned char intensite);
void changerEtatDels();
void pulserDELS();

//menu.cpp
void menuPrincipal();
void menuJeu();
int getNombreQuestionParNiveau();
int getDelaiQuestion();

//mode.cpp
void demo();
void recherche();

//robot.cpp
int robotAvance(double distance, int vitesseInitiale, int correctionActivee, int flagsArret);
int robotArc(double angle, int vitesseInitiale, int direction, int flagsArret);
int robotTourne(int angle, int vitesse, int flagsArret);
void setVitesseMoteurs(double gauche, double droite);
void arreterMoteurs();

typedef enum
{
	Rouge,
	Vert,
	Bleu,
	Jaune,
	Rose,
	Gris,
	Blanc,
	Noir,
} Couleur;

#endif
