#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <algorithm>

#define DELAI_QUESTION 1500

int jouer(unsigned int niveau, int deplacement);
int NiveauImpossible();

typedef enum
{
	Addition,
	Soustraction,
	Multiplication,
	Division,
	Comparaison,
	PlusPetit,
	PlusGrand,
	Puissance,
	Priorite,
	Priorite1,
	Priorite2,
	Priorite3,
	Priorite4,
	Priorite5,
	Priorite6,
	Priorite7,
	Priorite8,
	Euler,
	ExpSinPi,
} Operation;
