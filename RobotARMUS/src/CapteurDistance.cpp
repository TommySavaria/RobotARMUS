/*
============================================================================
 Name : CapteurDistance.cpp
 Author : Tommy Savaria
 Version : 1.0
 Description : Code main pour le sonar et le capteur de distance
============================================================================
*/

#include "headers/main.h"

#define GP2YO_INPUT 5

float lireTensionDistance()
{
	return (ANALOG_Read(GP2YO_INPUT) * 5.0) / 1023.0;
}

float lireDistance()
{
	return ((18 / (lireTensionDistance() - 0.25)) + 2.5) / 100.0;
}
