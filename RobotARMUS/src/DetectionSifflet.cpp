/*
============================================================================
 Name : DetectionSifflet.cpp
 Author : Guillaume Hivert, �tienne St-Pierre
 Description : Code de d�tection de sifflet
============================================================================
*/

#include "headers/main.h"

#define PCT 1.10

#define TIME 250

void DetectionSifflet()
{
	int base = 0;
	float seuil = 0;

	base = ANALOG_Read(2);
	seuil = ANALOG_Read(2) * PCT;

	LCD_Printf("Base = %d V, seuil = %f V \n", base, seuil);

	SYSTEM_ResetTimer();

	LCD_Printf("Attente du signal de depart\n");

	while (SYSTEM_ReadTimerMSeconds() < TIME)
	{
		if (ANALOG_Read(2) < seuil)
		{
			SYSTEM_ResetTimer();
		}
	}
	LCD_Printf("Son d�tect�");
}
