#include "headers/main.h"
#include "headers/jeu.h"

void demo()
{
	int i, j;

	LCD_ClearAndPrint("Mode demo");
	while(1)
	{
		i = nombreAleatoire(1, 10);
		j = nombreAleatoire(2);

		switch(robotTourne(6*i, 50, STOP_ON_KEYBOARD))
		{
			case 0:
			case 1:
			case 4:
				break;
			case 2: //IR Avant
				robotTourne(180, 60, 0);
				arreterMoteurs();
				jouer(1, 1);
				break;
			case 3: //IR Arri�re
				ecrireDEL(0);
				arreterMoteurs();
				jouer(1, 1);
				break;
			default:
				arreterMoteurs();
				return;
				break;
		}

		THREAD_MSleep(500);

		switch(robotAvance(0.2*i, 50, 1, STOP_ON_GP2 | STOP_ON_COLOR | STOP_ON_KEYBOARD))
		{
			case 0:
				break;
			case 1: //Capteur Distance
				robotArc(20, 50, j, 0);
				robotArc(70, 50, j, STOP_ON_GP2 | STOP_ON_COLOR);
				arreterMoteurs();
				break;
			case 2: //IR Avant
				robotTourne(180, 60, 0);
				arreterMoteurs();
				jouer(1, 1);
				break;
			case 3: //IR Arri�re
				arreterMoteurs();
				jouer(1, 1);
				break;
			case 4: //Capteur Couleur
				//Ruban gris d�tect�
				robotTourne(180, 60, 0);
				robotAvance(0.3, 50, 1, 0);
				arreterMoteurs();
				break;
			default:
				arreterMoteurs();
				return;
				break;
		}
		THREAD_MSleep(500);
	}
}
void recherche()
{
	int i, j;

	LCD_ClearAndPrint("Mode recherche");
	while(1)
	{
		i = nombreAleatoire(6);
		j = nombreAleatoire(2);

		switch(robotTourne(60*(i+1), 50, STOP_ON_IR_FRONT | STOP_ON_IR_BACK | STOP_ON_KEYBOARD))
		{
			case 0:
			case 1:
			case 4:
				break;
			case 2: //IR Avant
				robotTourne(180, 60, 0);
				arreterMoteurs();
				jouer(1, 1);
				break;
			case 3: //IR Arri�re
				arreterMoteurs();
				jouer(1, 1);
				break;
			case ESCAPE:
				arreterMoteurs();
				return;
			default:
				arreterMoteurs();
				jouer(1, 1);
				break;
		}

		THREAD_MSleep(500);

		switch(robotAvance(0.2*(i+1), 50, 1, STOP_ON_GP2 | STOP_ON_IR_FRONT | STOP_ON_IR_BACK | STOP_ON_COLOR | STOP_ON_KEYBOARD))
		{
			case 0:
				break;
			case 1: //Capteur Distance
				robotArc(20, 50, j, 0);
				robotArc(70, 50, j, STOP_ON_GP2 | STOP_ON_IR_FRONT | STOP_ON_IR_BACK | STOP_ON_COLOR);
				arreterMoteurs();
				break;
			case 2: //IR Avant
				robotTourne(180, 60, 0);
				arreterMoteurs();
				jouer(1, 1);
				break;
			case 3: //IR Arri�re
				arreterMoteurs();
				jouer(1, 1);
				break;
			case 4: //Capteur Couleur
				//Ruban gris d�tect�
				robotTourne(180, 60, 0);
				robotAvance(0.3, 50, 1, 0);
				arreterMoteurs();
				break;
			case ESCAPE:
				arreterMoteurs();
				return;
			default:
				arreterMoteurs();
				jouer(1, 1);
				break;
		}

		THREAD_MSleep(500);
	}
}
