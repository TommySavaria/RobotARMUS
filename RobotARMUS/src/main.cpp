/*
============================================================================
 Name : main.cpp
 Author : Guillaume Hivert, Anthony Fortin, Wendy Vasquez, Keven Deslandes, 
            Tommy Savaria, �tienne St-Pierre
 Version : 1.0
 Description : Code main pour le CRJ
============================================================================
*/

// Internal Include Files
#include "headers/main.h"

int main()
{
	unsigned char c;

	LCD_SetMonitorMode(MONITOR_OFF);
	AUDIO_SetVolume(50);

	while(1)
		menuPrincipal();
}
