#include "headers/main.h"

void demoancien()
{
	int ir;
	char c;
	bool quit = false;
	float tension;
	float distance;

	SYSTEM_ResetTimer();

	while(!quit)
	{
		if(lireCaractere() == ESCAPE)
			quit = true;

		if(SYSTEM_ReadTimerMSeconds() > 1000)
		{
			SYSTEM_ResetTimer();

			tension = (ANALOG_Read(5) / 1024.0) * 5.0;  // en V
			distance = 18 / (tension - 0.25) + 2.5;  // en cm

			if(tension < 0.25)
				LCD_ClearAndPrint("Distance en cm : Trop loin\n", distance);
			else
				LCD_ClearAndPrint("Distance en cm : %f\n", distance);

			LCD_Printf("Tension en V : %f\n", tension);

			if (tension >= 1.1)
			{
				LCD_Printf("\nPlanete trouvee!\n");
				THREAD_MSleep(5000);
			}

		}
	}

}
