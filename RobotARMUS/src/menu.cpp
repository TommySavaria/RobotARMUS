#include "headers/main.h"
#include "headers/jeu.h"

#include "audio_api.h"

void menuJeu();
void menuParametres();
void menuVolume();
void recherche();
void menuParametresJeu();
void afficherQuestionsParNiveau(int NombreQuestions);
void menuQuestionsParNiveau();

int volumeArmus = 50;
int nombreQuestionParNiveau = 5;
int delaiQuestion = 60;

int getNombreQuestionParNiveau()
{
	return nombreQuestionParNiveau;
}

int getDelaiQuestion()
{
	return delaiQuestion;
}

void afficheMenuPrincipal()
{
	LCD_ClearAndPrint("Menu principal\n\n");
	LCD_Printf("1. Demo\n");
	LCD_Printf("2. Recherche\n");
	LCD_Printf("3. Jouer\n");
	LCD_Printf("4. Parametres\n\n");
	LCD_Printf("Batterie: %.1fV", lireBatterie());
}

void menuPrincipal()
{
	static int timestamp = 0;

	//Lire touche
	switch(lireCaractere())
	{
		case '1':
			demo();
			break;
		case '2':
			recherche();
			break;
		case '3':
			menuJeu();
			break;
		case '4':
			menuParametres();
			break;
	}

	if(armus::system_ReadTimer_sec(timestamp) > 2)
	{
		timestamp = armus::system_GetTimeStamp();
		afficheMenuPrincipal();
	}
}

void menuJeu()
{
	unsigned char c;
	int r;
	bool quit = false;

	while(!quit)
	{
		LCD_ClearAndPrint("Jeu\n\n");
		LCD_Printf("Choisir votre difficulte (1-6)\n");
		LCD_Printf("Appuyez sur Escape pour quitter");

		c = lireCaractere();

		//Quitter
		if(c == ESCAPE || c == BACKSPACE)
			quit = true;

		else if(c == 'R')
			NiveauImpossible();

		//Niveau 1 � 6
		else if((c >= '1') && (c <= '6'))
		{
			r  = jouer(c - '0', 0);

			if(r == -1)
				quit = true;
		}
	}
}

void menuParametres()
{
	unsigned char c;
	bool quit = false;

	while(!quit)
	{
		LCD_ClearAndPrint("Parametres\n\n");
		LCD_Printf("1. Volume\n");
		LCD_Printf("2. Jeu\n");

		switch(lireCaractere())
		{
			case ESCAPE:
			case BACKSPACE:
				quit = true;
				break;
			case '1':
				try
				{
					menuVolume();
				}
				catch(AlsaAPIException e)
				{
					LCD_Printf("Erreur AlsaAPIException");
				}
				break;
			case '2':
				menuParametresJeu();
				break;
		}
	}
}

void afficheVolume(int volume)
{
	LCD_ClearAndPrint("Volume = %d\n\n", volumeArmus);
	LCD_Printf("Modifier: %d\n\n", volume);

	LCD_Printf("Enter pour confirmer\n");
	LCD_Printf("Esc pour annuler\n");
}


void menuParametresJeu()
{
	unsigned char c;
	bool done = false;
	LCD_ClearAndPrint("Menu Parametres de Jeu\n\n");
	LCD_Printf("1: Nombre questions par niveau");

	while(!done)
	{
		c = lireCaractere();

		if(c == ESCAPE)
		{
			//Quitte le menu
			done = true; //On quitte la boucle
		}
		if(c == 1)
		{
			menuQuestionsParNiveau();
		}
	}
}

void menuVolume()
{
	unsigned char c;
	int nombreVolume = 0;
	int precedent = 0;
	unsigned int bip;
	unsigned int son;
	bool done = false;

	afficheVolume(nombreVolume);

	while(!done)
	{
		c = lireCaractere();

		if(c == ESCAPE)
		{
			//Quitte le menu
			done = true; //On quitte la boucle
		}

		else if(c == BACKSPACE)
		{
			//Soustrait le dernier chiffre
			//Si c'est plus petit que 0, remm�ne � 0
			nombreVolume /= 10;
		}

		else if(c == ENTER)
		{
			//Quitte la fonction avec la valeur actuelle
			volumeArmus = nombreVolume;
			done = true;
		}

		else if(c == '0')
		{
			//Ajuste le nombre
			if(nombreVolume <= 10) //Limite � 100
				nombreVolume *= 10;
		}

		else if(isdigit(c)) //Si on appuie sur un chiffre
		{
			//Ajuste le nombre
			if(nombreVolume <= 9) //Limite � 2 chiffres
				nombreVolume = (nombreVolume * 10) + (c - '0');
		}

		if(nombreVolume != precedent)
		{
			afficheVolume(nombreVolume);
			AUDIO_SetVolume(nombreVolume);

			bip = AUDIO_PlayFile("beep.wav");

			precedent = nombreVolume;
		}
	}

	AUDIO_SetVolume(volumeArmus);
}

void menuQuestionsParNiveau()
{
	unsigned char c;
	int nombreQuestions = 0;
	int precedent = 0;

	bool done = false;

	afficherQuestionsParNiveau(nombreQuestions);

	while(!done)
	{
		c = lireCaractere();

		if(c == ESCAPE)
		{
			//Quitte le menu
			done = true; //On quitte la boucle
		}

		else if(c == BACKSPACE)
		{
			//Soustrait le dernier chiffre
			//Si c'est plus petit que 0, remm�ne � 0
			nombreQuestions /= 10;
		}

		else if(c == ENTER)
		{
			//Quitte la fonction avec la valeur actuelle
			nombreQuestionParNiveau = nombreQuestions;
			done = true;
		}

		else if(c == '0')
		{
			//Ajuste le nombre
			if(nombreQuestions <= 10) //Limite � 100
				nombreQuestions *= 10;
		}

		else if(isdigit(c)) //Si on appuie sur un chiffre
		{
			//Ajuste le nombre
			if(nombreQuestions <= 9) //Limite � 2 chiffres
				nombreQuestions = (nombreQuestions * 10) + (c - '0');
		}

		if(nombreQuestions != precedent)
		{
			afficherQuestionsParNiveau(nombreQuestions);

			precedent = nombreQuestions;
		}
	}

	AUDIO_SetVolume(volumeArmus);
}

void afficherQuestionsParNiveau(int nombreQuestions)
{
	LCD_ClearAndPrint("Questions par niveau:", getNombreQuestionParNiveau());
	LCD_Printf("Modifier: %d\n\n", nombreQuestions);

	LCD_Printf("Enter pour confirmer\n");
	LCD_Printf("Esc pour annuler\n");
}
