#include "headers/main.h"

#define ENTREE_BATTERIE 1
#define LED_BATTERIE 10
#define FACTEUR_ECHELLE_BATTERIE 60.2
#define TENSION_MINIMUM 11.0

double lireBatterie()
{
	return ANALOG_Read(ENTREE_BATTERIE)/FACTEUR_ECHELLE_BATTERIE;
}

void allumeDELBasseTension()
{
	DIGITALIO_Write(LED_BATTERIE, ON);
}

void verifierTension()
{
	double tension = lireBatterie();

	if(tension < TENSION_MINIMUM)
	{
		setVitesseMoteurs(0,0);
		allumeDELBasseTension();
		LCD_ClearAndPrint("Batterie basse! %.1f", tension);
		while(lireBatterie() < TENSION_MINIMUM);
	}
}
