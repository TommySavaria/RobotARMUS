/*
============================================================================
Name : TestEntrees.cpp
 Author :
 Version :
 Description : Hello world - Exe source file
============================================================================
*/

// Include Files

#include <libarmus.h>

// Global Functions
void affichageEncodeurs();
void avancerMoteurs();
void reculerMoteurs();
void changerEncodeurALire();

THREAD moteur;
int encodeur_a_lire = ENCODER_LEFT;

int main()
{
	int bmp_gauche = 0, bmp_droit = 0, bmp_avant = 0, bmp_arriere = 0;
	int ir, ir_gauche = 0, ir_droit = 0;

	while(1)
	{
		//Bumper droit
		if(!bmp_droit && (DIGITALIO_Read(BMP_RIGHT) == 1))
		{
			LCD_Printf("Bumper droit\n");
			affichageEncodeurs();
		}
		bmp_droit = DIGITALIO_Read(BMP_RIGHT);

		//Bumper gauche
		if(!bmp_gauche && (DIGITALIO_Read(BMP_LEFT) == 1))
		{
			LCD_Printf("Bumper gauche\n");
			changerEncodeurALire();
		}
		bmp_gauche = DIGITALIO_Read(BMP_LEFT);

		//Bumper avant
		if(!bmp_avant && (DIGITALIO_Read(BMP_FRONT) == 1))
		{
			LCD_Printf("Bumper avant\n");
			moteur = THREAD_CreateSimple(reculerMoteurs);
		}
		bmp_avant = DIGITALIO_Read(BMP_FRONT);

		//Bumper arriere
		if(!bmp_arriere && (DIGITALIO_Read(BMP_REAR) == 1))
		{
			LCD_Printf("Bumper arriere\n");
			moteur = THREAD_CreateSimple(avancerMoteurs);
		}
		bmp_arriere = DIGITALIO_Read(BMP_REAR);

		ir = IR_Detect(IR_FRONT);

		if(!ir_droit && (ir & 0x01))
		{
			LCD_Printf("Infrarouge droite\n");
		}
		ir_droit = (ir & 0x01);
		if(!ir_gauche && (ir & 0x02))
		{
			LCD_Printf("Infrarouge gauche\n");
		}
		ir_gauche = (ir & 0x02);

	}

	return 0;
}

void affichageEncodeurs()
{
	LCD_Printf("Encodeur gauche: %d\nEncodeur droit: %d\n", ENCODER_Read(ENCODER_LEFT), ENCODER_Read(ENCODER_RIGHT));
}


void changerEncodeurALire()
{
	if(encodeur_a_lire == ENCODER_LEFT)
	{
		encodeur_a_lire = ENCODER_RIGHT;
		LCD_Printf("Encodeur a lire: droite\n");
	}
	else
	{
		encodeur_a_lire = ENCODER_LEFT;
		LCD_Printf("Encodeur a lire: gauche\n");
	}
}

void avancerMoteurs()
{
	int compteur = 0;
	MOTOR_SetSpeed(MOTOR_LEFT, 50);
	MOTOR_SetSpeed(MOTOR_RIGHT, 50);

	while (compteur < 64)
	{
		compteur = compteur + ENCODER_Read(encodeur_a_lire);
	}

	MOTOR_SetSpeed(MOTOR_LEFT, 0);
	MOTOR_SetSpeed(MOTOR_RIGHT, 0);
	THREAD_Destroy(&moteur);
}

void reculerMoteurs()
{
	MOTOR_SetSpeed(MOTOR_LEFT, -50);
	MOTOR_SetSpeed(MOTOR_RIGHT, -50);
	THREAD_MSleep(10000);
	MOTOR_SetSpeed(MOTOR_LEFT, 0);
	MOTOR_SetSpeed(MOTOR_RIGHT, 0);
	THREAD_Destroy(&moteur);
}
