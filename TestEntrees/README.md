# Programme des tests des entrées #
## Bumpers ##
Appuyez sur un des bumpers pour qu'il s'affiche à l'écran
## Infrarouge ##
Passez la main devant un des capteurs pour qu'il s'affiche à l'écran
## Encodeurs ##
Appuyez sur bumper gauche ou droit pour afficher la valeur des encodeurs et les remettre à 0
## Moteurs ##
Appuyez sur bumper arrière pour faire avancer les moteurs. Appuyez sur bumper avant pour faire reculer les moteurs